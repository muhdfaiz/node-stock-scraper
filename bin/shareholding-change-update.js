const axios = require('axios');
const https = require('https');
const cheerio = require('cheerio');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
require('dotenv').config();
var moment = require('moment');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const initDBCOnnection = () => {
	const database = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: 'mysql',
		"define": {
			"underscored": true,
		},
		dialectOptions: {
			supportBigNumbers: true,
			dateStrings: true,
			typeCast: true,
			timezone: "+08:00"
		},
		// logging: console.log
		timezone: "+08:00", //for writing to database
		operatorsAliases: false
	});

	return database;
};

const initStockModel = (db) => {
	const Stocks = db.define('stocks', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		parent_code: Sequelize.STRING,
		warrant: Sequelize.BOOLEAN,
		stock_code: Sequelize.STRING,
		stock_name: Sequelize.STRING,
		name: Sequelize.STRING,
		company_website: Sequelize.STRING,
		market: Sequelize.STRING,
		sector: Sequelize.STRING,
		tags: Sequelize.JSON,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		buy_volume: Sequelize.BIGINT(20),
		buy: Sequelize.DECIMAL(8,4),
		sell_volume: Sequelize.BIGINT(20),
		sell: Sequelize.DECIMAL(8,4),
		shariah: Sequelize.BOOLEAN,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock' });

	return Stocks;
};

const initShareholdingChangeModel = (db) => {
	return db.define('stock_shareholding_changes', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		stock_id: Sequelize.INTEGER,
		announced: Sequelize.DATEONLY,
		date_change: Sequelize.DATEONLY,
		type: Sequelize.STRING,
		shares: Sequelize.STRING,
		name: Sequelize.STRING,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock_shareholding_changes' });
};

const generateURLList = async (stocks) => {
	const urlList = [];
	for (let i= 0; i < stocks.length; i++) {
		urlList.push("https://www.klsescreener.com/v2/stocks/view/" +  stocks[i].stock_code);
	}

	return urlList;
};


const shareholdingChangeUpdate = async () => {
	let database = initDBCOnnection();

	let Stock = initStockModel(database);
	let ShareholdingChange = initShareholdingChangeModel(database);

	await Stock.findAll({
		attributes: ['id', 'stock_name', 'stock_code']
	}).then(async (stocks) => {
		let urlList = await generateURLList(stocks);

		await Promise.map(urlList, (url, index, length) => {
			return axios.get(url, {
				headers: {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
					'authority': 'www.klsescreener.com',
					'method': 'GET',
					'Accept-Encoding': '*',
					'Cache-Control': 'no-cache',
					Pragma: 'no-cache',
					cookie: '_ga=GA1.2.1035241416.1578901356; SL_C_23361dd035530_KEY=921a42a437a7053aa25ac2ed392a5181864cfaa5; __cfduid=d15c025ad3e5762fb8c497261d880e00b1584697681; _gid=GA1.2.1772333888.1586457493; headers=[null%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2C0]; __unam=bb19df2-16f9ddabba5-d2e6f17-108',
					Connection: 'keep-alive',
					Host: 'www.klsescreener.com',
					'Sec-Fetch-Dest': 'document',
					'Sec-Fetch-Mode': 'navigate',
					'Sec-Fetch-Site': 'none',
					'Upgrade-Insecure-Requests': '1',
					'Sec-Fetch-User': '?1',
					'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
				},
				httpsAgent: new https.Agent({
					rejectUnauthorized: false,
				}),
			}).then((response) => {
				const $ = cheerio.load(response.data);

				let shareholdingChanges = [];

				$('#shareholding_changes .table tbody tr').each(function (index1, element) {
					if ($(element).find('td').length <= 0) {
						return null;
					}

					if ($(element).find('td').first().text() === 'No financial reports found yet.') {
						return null;
					}

					let announced = $(element).find('td').eq(0).text();
					announced = moment(announced, 'DD MMM YYYY').format('YYYY-MM-DD');

					if (moment(announced, 'DD MMM YYYY').isAfter(moment())) {
						return;
					}

					let dateChange = $(element).find('td').eq(1).text();
					dateChange = moment(dateChange, 'DD MMM YYYY').format('YYYY-MM-DD');

					let type = $(element).find('td').eq(2).text().trim();
					let shares = $(element).find('td').eq(3).text().trim();
					let name = $(element).find('td').eq(4).text().trim();

					shareholdingChanges.push({
						'announced': announced,
						'date_change': dateChange,
						'type': type,
						'shares': shares,
						'name': name,
						'stock_id': stocks[index].id,
					});
				});

				ShareholdingChange.bulkCreate(shareholdingChanges);
			}).catch(error => {
				//console.log(error);
			});
		}, {
			concurrency: 1
		});
	});

};

shareholdingChangeUpdate();
