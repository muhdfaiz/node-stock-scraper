const axios = require('axios');
const cheerio = require('cheerio');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
require('dotenv').config({ path: __dirname + '/../.env' });
var moment = require('moment');

const getTotalPage = () => {
	return axios.get('https://www.bursamalaysia.com/market_information/equities_prices?per_page=50&page=1', {
		headers: {
			Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
			'Accept-Encoding': 'gzip, deflate, br',
			Connection: 'keep-alive',
			Host: 'www.bursamalaysia.com',
			'Sec-Fetch-Dest': 'document',
			'Sec-Fetch-Mode': 'navigate',
			'Sec-Fetch-Site': 'none',
			'Sec-Fetch-User': '?1',
			'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
		}
	}).then((response) => {
		const $ = cheerio.load(response.data);

		let totalPagination = $('.pagination li .page-link.footer-page-link').length;

		return parseInt($('.pagination li .page-link.footer-page-link').eq(totalPagination - 2).text());
	});
};

const generateURLList = async () => {
	const urlList = [];
	let totalPage = await getTotalPage();

	for (let i= 1; i <= totalPage; i++) {
		urlList.push('https://www.bursamalaysia.com/market_information/equities_prices?per_page=50&page=' + i);
	}

	return urlList;
};

const initDBCOnnection = () => {
	const database = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: 'mysql',
		"define": {
			"underscored": true,
		},
		dialectOptions: {
			supportBigNumbers: true,
			dateStrings: true,
			typeCast: true,
			timezone: "+08:00"
		},
		// logging: console.log
		timezone: "+08:00", //for writing to database
		operatorsAliases: false
	});

	return database;
};

const initStockModel = (db) => {
	const Stocks = db.define('stocks', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		parent_code: Sequelize.STRING,
		warrant: Sequelize.BOOLEAN,
		stock_code: Sequelize.STRING,
		stock_name: Sequelize.STRING,
		name: Sequelize.STRING,
		company_website: Sequelize.STRING,
		market: Sequelize.STRING,
		sector: Sequelize.STRING,
		tags: Sequelize.JSON,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		buy_volume: Sequelize.BIGINT(20),
		buy: Sequelize.DECIMAL(8,4),
		sell_volume: Sequelize.BIGINT(20),
		sell: Sequelize.DECIMAL(8,4),
		shariah: Sequelize.BOOLEAN,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock' });

	return Stocks;
};

const initStockPriceHistoryModel = (db) => {
	const StockPriceHistory = db.define('stock_price_histories', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		date: Sequelize.DATEONLY,
		stock_id: Sequelize.INTEGER,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock_price_histories' });

	return StockPriceHistory;
};

const scrapeStockList = async () => {

	let database = initDBCOnnection();

	let Stock = initStockModel(database);
	let StockPriceHistory = initStockPriceHistoryModel(database);

	let urlList = await generateURLList();

	await Promise.map(urlList, (url, index, length) => {
		return axios.get(url, {
			headers: {
				'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
				'Cache-Control': 'no-cache',
				Pragma: 'no-cache',
				'Referer': 'https://www.bursamalaysia.com/market_information/equities_prices',
				Cookie: '_ga=GA1.2.584247873.1573135051; __utmz=133856029.1573202414.8.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utma=133856029.584247873.1573135051.1573876438.1573878842.15; saved_link=equities_prices,indices_prices,derivatives_prices,shariah_compliant_equities_prices,company_announcements; cookieconsent_status=dismiss; _gid=GA1.2.1828034092.1586361917; _locomotiveapp_session=U3hlSEZBdUN3aUhlcjNoQW5RaUEzVUpsV2VKQVRZVTJ5K0dHeGJ4S0RNcUc4c1JWVXR4UDFCcGRYN0J1cFBZSW5ZdWdsTGdmTUFkM1BWZTFON1NKSlRGVXJyQkR3THI3RlZvWkQ0V0NndnZ5TFVXdUYyR2JCQkp3VUI5TStMQlVranY2WmZGZ1VQM25IZ0htUi9QNy9nPT0tLUJuNlRSMXNHZ0F2SFZzM3B2M3VYMUE9PQ%3D%3D--d8190085beb3d364042ea3e77612bd9e2f33e8dd; TS01238bec=01db214deb4c18c281330ef220541fed27001fd0cd99697c2e99f0e76022741e72fec60cef64a63a63a4a02053582402750cf2218d11ad07224d6bc93cc8194dc23542dd03; TS6c22aae8027=0886c5af14ab20000eac8d8902c3298211a278b7fa73b681b0d21b5fdf24577e259254588c117134083ed1f779113000e4741d0bc3f231d1a3190938b54fb107da8de3828c681dc8220cabcde14f370c7dd38b973c0bb9d5ce092c2b9c7b6d2d; _gat_gtag_UA_147934313_1=1',
				Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
				'Accept-Encoding': 'gzip, deflate, br',
				Connection: 'keep-alive',
				Host: 'www.bursamalaysia.com',
				'Sec-Fetch-Dest': 'document',
				'Sec-Fetch-Mode': 'navigate',
				'Sec-Fetch-Site': 'same-origin',
				'Upgrade-Insecure-Requests': '1',
				'Sec-Fetch-User': '?1',
				'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
			}
		}).then((response) => {
			let $ = cheerio.load(response.data);

			return $('table.datatable-striped tbody tr').each(function (index, element) {
				let stockCode = $(this).find('td').eq(2).text();

				let shariah = $(this).find('td').eq(1).text();

				let stockName = shariah.replace('[S]', '').trim();

				if (shariah.includes("[S]")) {
					shariah = 1;
				} else {
					shariah = 0;
				}

				let detailURL = 'https://www.bursamalaysia.com/trade/trading_resources/listing_directory/company-profile?stock_code=' + stockCode;

				return  axios.get(detailURL, {
					headers: {
						Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
						'Accept-Encoding': 'gzip, deflate, br',
						Connection: 'keep-alive',
						Host: 'www.bursamalaysia.com',
						'Sec-Fetch-Dest': 'document',
						'Sec-Fetch-Mode': 'navigate',
						'Sec-Fetch-Site': 'none',
						'Sec-Fetch-User': '?1',
						'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
					}
				}).then((response) => {
					let $ = cheerio.load(response.data);

					let companyName = $('.bigbgimg h5').text();

					let market = $('.bigbgimg > .row:first-child > div:first-child .row div').eq(0).text();
					market = market.trim().replace("Market:", "");

					if (market !== 'Structured Warrants') {
						let sector = $('.bigbgimg .row div div:nth-child(3) div').eq(0).text();
						sector = sector.trim().replace("Sector:", "");

						let parent = null;
						let warrant = isNaN(stockCode);

						if (warrant) {
							warrant = 1;
							parent = parseInt(stockCode);
						} else {
							warrant = 0;
						}

						let companyWebsite = null;

						if (!warrant) {
							companyWebsite = $('.btn.btn-block.btn-effect.btn-white').eq(0).text();

							if (companyWebsite === "Company Website") {
								companyWebsite = $('.btn.btn-block.btn-effect.btn-white').attr("href");
							}
						}

						let change = $('#pills-tabContent table').eq(0).find('tr').eq(1).find('td').text().trim();

						if (change === '-') {
							change = null;
						}

						let percentageChange = $('#pills-tabContent table').eq(0).find('tr').eq(2).find('td').text().trim();

						if (percentageChange === '-') {
							percentageChange = null;
						}

						let volume = $('#pills-tabContent table').eq(0).find('tr').eq(3).find('td').text().trim();

						volume = volume.replace(',', '').replace(',', '');

						if (volume === '-') {
							volume = null;
						} else {
							volume = volume + '00';
						}

						let last = $('#pills-tabContent .company-last-done h3').eq(1).text().trim();

						if (last === '-') {
							last = null;
						}

						let previousClose = $('#pills-tabContent table').eq(2).find('tr').eq(0).find('td').text().trim();

						if (previousClose === '-') {
							previousClose = null;
						}

						let open = $('#pills-tabContent table').eq(2).find('tr').eq(1).find('td').text().trim();

						if (open === '-') {
							open = null;
						}

						let high = $('#pills-tabContent table').eq(2).find('tr').eq(2).find('td').text().trim();

						if (high === '-') {
							high = null;
						}

						let low = $('#pills-tabContent table').eq(2).find('tr').eq(3).find('td').text().trim();

						if (low === '-') {
							low = null;
						}

						let todayDateTime = moment().utcOffset('+0800').format('YYYY-MM-DD HH:mm:ss');
						let todayDate = moment().utcOffset('+0800').format('YYYY-MM-DD');

						const data = {
							'parent_code': parent,
							'warrant': warrant,
							'stock_code': stockCode,
							'stock_name': stockName,
							'name': companyName,
							'company_website': companyWebsite,
							'market': market,
							'sector': sector,
							'tags': null,
							'last': last,
							'open': open,
							'close': previousClose,
							'high': high,
							'low': low,
							'change': change,
							'percentage_change': percentageChange,
							'volume': volume,
							'buy_volume': null,
							'buy': null,
							'sell_volume': null,
							'sell': null,
							'shariah': shariah,
							'created_at': todayDateTime,
							'updated_at': todayDateTime
						};

						try {
							let created = Stock.findOne({
								where: {stock_code: stockCode},
								attributes: ['id', 'stock_code']
							}).then(stock => {
								if (!stock) {
									Stock.create(data).then(createdStock => {
										let priceHistorydata = {
											'stock_id': createdStock.id,
											'date': todayDate,
											'last': last,
											'open': open,
											'close': last,
											'high': high,
											'low': low,
											'change': change,
											'percentage_change': percentageChange,
											'volume': volume,
											'created_at': todayDateTime,
											'updated_at': todayDateTime
										};

										StockPriceHistory.create(priceHistorydata);
									});
								} else {
									Stock.update(data, {where: {id: stock.id}}).then(() => {
										StockPriceHistory.findOne({
											where: {date: todayDate, stock_id: stock.id},
											attributes: ['id', 'date', 'stock_id']
										})
											.then (stockPriceHistory => {
												let priceHistorydata = {
													'stock_id': stock.id,
													'date': todayDate,
													'last': last,
													'open': open,
													'close': last,
													'high': high,
													'low': low,
													'change': change,
													'percentage_change': percentageChange,
													'volume': volume,
													'created_at': todayDateTime,
													'updated_at': todayDateTime
												};

												if (stockPriceHistory) {
													StockPriceHistory.update(priceHistorydata, {where: {id: stockPriceHistory.id}})
												} else {
													StockPriceHistory.create(priceHistorydata);
												}
											});
									})
								}

								return null;
							});
						} catch (e) {
							console.log(e)
						}
					}
				}).catch(function (err) {
					console.log(err);
				});
			});
		}).catch(function (err) {
			console.log(err);
		});
	}, {
		concurrency: 1
	});
};

scrapeStockList();
