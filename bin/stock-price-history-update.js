const axios = require('axios');
const https = require('https');
const cheerio = require('cheerio');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
require('dotenv').config();
var moment = require('moment');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const initDBCOnnection = () => {
	const database = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: 'mysql',
		"define": {
			"underscored": true,
		},
		dialectOptions: {
			supportBigNumbers: true,
			dateStrings: true,
			typeCast: true,
			timezone: "+08:00"
		},
		// logging: console.log
		timezone: "+08:00", //for writing to database
		operatorsAliases: false
	});

	return database;
};

const initStockModel = (db) => {
	const Stocks = db.define('stocks', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		parent_code: Sequelize.STRING,
		warrant: Sequelize.BOOLEAN,
		stock_code: Sequelize.STRING,
		stock_name: Sequelize.STRING,
		name: Sequelize.STRING,
		company_website: Sequelize.STRING,
		market: Sequelize.STRING,
		sector: Sequelize.STRING,
		tags: Sequelize.JSON,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		buy_volume: Sequelize.BIGINT(20),
		buy: Sequelize.DECIMAL(8,4),
		sell_volume: Sequelize.BIGINT(20),
		sell: Sequelize.DECIMAL(8,4),
		shariah: Sequelize.BOOLEAN,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock' });

	return Stocks;
};

const initStockPriceHistoryModel = (db) => {
	const StockPriceHistory = db.define('stock_price_histories', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		date: Sequelize.DATEONLY,
		stock_id: Sequelize.INTEGER,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock_price_histories' });

	return StockPriceHistory;
};

const chunk = (arr, size) => {
	return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
		arr.slice(i * size, i * size + size)
	);
};

const generateURLList = async (stocks) => {
	const urlList = [];
	for (let i= 0; i < stocks.length; i++) {
		urlList.push("https://www.isaham.my/history?symbol=" +  stocks[i].stock_name + "&resolution=D&from=1525925700&to=1557029760");
	}

	return urlList;
};


const scrapeStockPriceHistories = async () => {
	let database = initDBCOnnection();

	let Stock = initStockModel(database);
	let StockPriceHistory = initStockPriceHistoryModel(database);

	await Stock.findAll({
		attributes: ['id', 'stock_name']
	}).then(async (stocks) => {
		let urlList = await generateURLList(stocks);

		await Promise.map(urlList, (url, index, length) => {
			return axios.get(url, {
				headers: {
					'authority': 'www.klsescreener.com',
					'method': 'GET',
					'path': '/v2/trading_view/history?symbol=9261&resolution=D&from=1555353824&to=1586457884',
					'scheme': 'https',
					'Accept-Encoding': 'gzip, deflate, br',
					'Cache-Control': 'no-cache',
					Pragma: 'no-cache',
					Cookie: '_ga=GA1.2.584247873.1573135051; __utmz=133856029.1573202414.8.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utma=133856029.584247873.1573135051.1573876438.1573878842.15; saved_link=equities_prices,indices_prices,derivatives_prices,shariah_compliant_equities_prices,company_announcements; cookieconsent_status=dismiss; _gid=GA1.2.1828034092.1586361917; _locomotiveapp_session=U3hlSEZBdUN3aUhlcjNoQW5RaUEzVUpsV2VKQVRZVTJ5K0dHeGJ4S0RNcUc4c1JWVXR4UDFCcGRYN0J1cFBZSW5ZdWdsTGdmTUFkM1BWZTFON1NKSlRGVXJyQkR3THI3RlZvWkQ0V0NndnZ5TFVXdUYyR2JCQkp3VUI5TStMQlVranY2WmZGZ1VQM25IZ0htUi9QNy9nPT0tLUJuNlRSMXNHZ0F2SFZzM3B2M3VYMUE9PQ%3D%3D--d8190085beb3d364042ea3e77612bd9e2f33e8dd; TS01238bec=01db214deb4c18c281330ef220541fed27001fd0cd99697c2e99f0e76022741e72fec60cef64a63a63a4a02053582402750cf2218d11ad07224d6bc93cc8194dc23542dd03; TS6c22aae8027=0886c5af14ab20000eac8d8902c3298211a278b7fa73b681b0d21b5fdf24577e259254588c117134083ed1f779113000e4741d0bc3f231d1a3190938b54fb107da8de3828c681dc8220cabcde14f370c7dd38b973c0bb9d5ce092c2b9c7b6d2d; _gat_gtag_UA_147934313_1=1',
					Connection: 'keep-alive',
					Host: 'www.isaham.my',
					'Sec-Fetch-Dest': 'document',
					'Sec-Fetch-Mode': 'navigate',
					'Sec-Fetch-Site': 'same-origin',
					'Upgrade-Insecure-Requests': '1',
					'Sec-Fetch-User': '?1',
					'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
				},
				httpsAgent: new https.Agent({
					rejectUnauthorized: false,
				}),
			}).then((response) => {
				let priceHistories = response.data;

				Stock.findOne({where: {stock_name: stocks[index].stock_name}}).then(stock => {
					if (!stock) {
						return;
					}

					let inputs = [];

					priceHistories.t.forEach((timestamp, index) => {
						timestamp = parseInt(timestamp);

						let date = moment.unix(timestamp).format('YYYY-MM-DD');
						console.log(date);
						let open = priceHistories['o'][index];
						let low = priceHistories['l'][index];
						let close = priceHistories['c'][index];
						let high = priceHistories['h'][index];
						let volume = priceHistories['v'][index];
						let last = null;
						let change = null;
						let changePercentage = null;

						if (index !== 0) {
							last = priceHistories['c'][index - 1];
							change = (close - last).toFixed(4);

							if (last <= 0) {
								changePercentage = 0;
							} else {
								changePercentage = ((change / last) * 100).toFixed(2);
							}
						}

						let input = {
							'stock_id': stock.id,
							'date': date,
							'last': last,
							'open': open,
							'close': close,
							'high': high,
							'low': low,
							'change': change,
							'percentage_change': changePercentage,
							'volume': parseInt(volume),
						};

						inputs.push(input)
					});

					let chunkData = chunk(inputs, 500);

					chunkData.forEach((data, index) => {
						StockPriceHistory.bulkCreate(data);
					})
				});
			}).catch(error => {
				//console.log(error);
			});
		}, {
			concurrency: 1
		});
	});

};

scrapeStockPriceHistories();
