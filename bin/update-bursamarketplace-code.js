const axios = require('axios');
const https = require('https');
const cheerio = require('cheerio');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
require('dotenv').config();
var moment = require('moment');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const initDBCOnnection = () => {
	const database = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: 'mysql',
		"define": {
			"underscored": true,
		},
		dialectOptions: {
			supportBigNumbers: true,
			dateStrings: true,
			typeCast: true,
			timezone: "+08:00"
		},
		// logging: console.log
		timezone: "+08:00", //for writing to database
		operatorsAliases: false
	});

	return database;
};

const initStockModel = (db) => {
	const Stocks = db.define('stocks', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		bursamarketplace_code: Sequelize.STRING,
		parent_code: Sequelize.STRING,
		warrant: Sequelize.BOOLEAN,
		stock_code: Sequelize.STRING,
		stock_name: Sequelize.STRING,
		name: Sequelize.STRING,
		company_website: Sequelize.STRING,
		market: Sequelize.STRING,
		sector: Sequelize.STRING,
		tags: Sequelize.JSON,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		buy_volume: Sequelize.BIGINT(20),
		buy: Sequelize.DECIMAL(8,4),
		sell_volume: Sequelize.BIGINT(20),
		sell: Sequelize.DECIMAL(8,4),
		shariah: Sequelize.BOOLEAN,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock' });

	return Stocks;
};

const generateURLList = async (stocks) => {
	const urlList = [];
	for (let i= 0; i < stocks.length; i++) {
		urlList.push("http://www.bursamarketplace.com/index.php?tpl=th001_search_ajax&string=" +  stocks[i].stock_code);
	}

	return urlList;
};


const updateBursaMarketplaceCode = async () => {
	let database = initDBCOnnection();

	let Stock = initStockModel(database);

	await Stock.findAll({
		where: {'bursamarketplace_code': null, parent_code: null},
		attributes: ['id', 'stock_name', 'stock_code']
	}).then(async (stocks) => {
		let urlList = await generateURLList(stocks);

		await Promise.map(urlList, (url, index, length) => {
			return axios.get(url, {
				headers: {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
					'Accept-Encoding': 'gzip, deflate',
					'Cache-Control': 'no-cache',
					'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
					Pragma: 'no-cache',
					cookie: 'ga=GA1.2.1417721042.1583132267; _fbp=fb.1.1583132268372.297777438; _gid=GA1.2.920209566.1586544623; PHPSESSID=1506a6993f103643f88f3eaf67c1c8e0; TS015ecd7b=0184ec8066333e066a19a2dce9da5d7e6a817aed8791a2bdcc5072da3e321b2647b2a853e60e172e1f7d37eab548da3f969f81526d; TS01497b45=0184ec8066fd01000414c3769eed6db0ae3fd17e4591a2bdcc5072da3e321b2647b2a853e6043a21b35f289b974beb81cf0f8557243c0d789c7ff83388a7773e87f125725b',
					'Connection': 'keep-alive',
					Host: 'www.bursamarketplace.com',
					'Upgrade-Insecure-Requests': '1',
					'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
				},
				httpsAgent: new https.Agent({
					rejectUnauthorized: false,
				}),
			}).then((response) => {
				const $ = cheerio.load(response.data);

				let url = $('a').attr('href');

				let code = url.substring(url.lastIndexOf('/') + 1)

				if (code) {
					Stock.update({'bursamarketplace_code': code}, {where: {id: stocks[index].id}})
				}

			}).catch(error => {
				//console.log(error);
			});
		}, {
			concurrency: 1
		});
	});

};

updateBursaMarketplaceCode();
