const axios = require('axios');
const https = require('https');
const cheerio = require('cheerio');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
require('dotenv').config();
var moment = require('moment');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const initDBCOnnection = () => {
	const database = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: 'mysql',
		"define": {
			"underscored": true,
		},
		dialectOptions: {
			supportBigNumbers: true,
			dateStrings: true,
			typeCast: true,
			timezone: "+08:00"
		},
		// logging: console.log
		timezone: "+08:00", //for writing to database
		operatorsAliases: false
	});

	return database;
};

const initStockModel = (db) => {
	const Stocks = db.define('stocks', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		parent_code: Sequelize.STRING,
		warrant: Sequelize.BOOLEAN,
		stock_code: Sequelize.STRING,
		stock_name: Sequelize.STRING,
		name: Sequelize.STRING,
		company_website: Sequelize.STRING,
		market: Sequelize.STRING,
		sector: Sequelize.STRING,
		tags: Sequelize.JSON,
		last: Sequelize.DECIMAL(8,4),
		open: Sequelize.DECIMAL(8,4),
		close: Sequelize.DECIMAL(8,4),
		high: Sequelize.DECIMAL(8,4),
		low: Sequelize.DECIMAL(8,4),
		change: Sequelize.DECIMAL(8,4),
		percentage_change: Sequelize.DECIMAL(8,4),
		volume: Sequelize.BIGINT(20),
		buy_volume: Sequelize.BIGINT(20),
		buy: Sequelize.DECIMAL(8,4),
		sell_volume: Sequelize.BIGINT(20),
		sell: Sequelize.DECIMAL(8,4),
		shariah: Sequelize.BOOLEAN,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock' });

	return Stocks;
};

const initYearlyReportModel = (db) => {
	return db.define('stock_yearly_reports', {
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
		stock_id: Sequelize.INTEGER,
		eps: Sequelize.DOUBLE(8,2),
		revenue: Sequelize.DECIMAL(14,4),
		net_income: Sequelize.DECIMAL(14,4),
		financial_date: Sequelize.DATEONLY,
		createdAt: Sequelize.DATE,
		updatedAt: Sequelize.DATE,
	}, { Sequelize, modelName: 'stock_yearly_reports' });
};

const chunk = (arr, size) => {
	return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
		arr.slice(i * size, i * size + size)
	);
};

const generateURLList = async (stocks) => {
	const urlList = [];
	for (let i= 0; i < stocks.length; i++) {
		urlList.push("https://www.klsescreener.com/v2/stocks/view/" +  stocks[i].stock_code);
	}

	return urlList;
};


const scrapeQuarterReports = async () => {
	let database = initDBCOnnection();

	let Stock = initStockModel(database);
	let YearlyReport = initYearlyReportModel(database);

	await Stock.findAll({
		attributes: ['id', 'stock_name', 'stock_code']
	}).then(async (stocks) => {
		let urlList = await generateURLList(stocks);

		await Promise.map(urlList, (url, index, length) => {
			return axios.get(url, {
				headers: {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
					'authority': 'www.klsescreener.com',
					'method': 'GET',
					'Accept-Encoding': '*',
					'Cache-Control': 'no-cache',
					Pragma: 'no-cache',
					cookie: '_ga=GA1.2.1035241416.1578901356; SL_C_23361dd035530_KEY=921a42a437a7053aa25ac2ed392a5181864cfaa5; __cfduid=d15c025ad3e5762fb8c497261d880e00b1584697681; _gid=GA1.2.1772333888.1586457493; headers=[null%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2C0]; __unam=bb19df2-16f9ddabba5-d2e6f17-108',
					Connection: 'keep-alive',
					Host: 'www.klsescreener.com',
					'Sec-Fetch-Dest': 'document',
					'Sec-Fetch-Mode': 'navigate',
					'Sec-Fetch-Site': 'none',
					'Upgrade-Insecure-Requests': '1',
					'Sec-Fetch-User': '?1',
					'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
				},
				httpsAgent: new https.Agent({
					rejectUnauthorized: false,
				}),
			}).then((response) => {
				const $ = cheerio.load(response.data);

				let yearlyReports = [];

				$('#annual .table tbody tr').each(function (index1, element) {
					if ($(element).find('td').length <= 0) {
						return null;
					}

					if ($(element).find('td').first().text() === 'No financial reports found yet.') {
						return null;
					}

					let revenue = $(element).find('td').eq(1).text();
					revenue = revenue.replace(/,/g, "") + '000';

					let netIncome = $(element).find('td').eq(2).text();
					netIncome = netIncome.replace(/,/g, "")  + '000';
					let eps = $(element).find('td').eq(3).text();

					let financialDate = $(element).find('td').eq(0).text();
					financialDate = moment(financialDate).format('YYYY-MM-DD');

					yearlyReports.push({
						'eps': eps,
						'revenue': revenue,
						'net_income': netIncome,
						'financial_date': financialDate,
						'stock_id': stocks[index].id,
					});
				});

				yearlyReports.forEach((yearlyReport, index) => {
					let created = YearlyReport.findOne({
						where: {stock_id: yearlyReport.stock_id, financial_date: yearlyReport.financial_date},
						attributes: ['id']
					}).then(report => {
						if (!report) {
							YearlyReport.create(yearlyReport);
						}
					});
				});
			}).catch(error => {
				//console.log(error);
			});
		}, {
			concurrency: 1
		});
	});

};

scrapeQuarterReports();
